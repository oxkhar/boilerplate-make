
## Params
PHP_CS_FIXER_VERSION ?= 3.2.1
PHP_CS_FIXER_OPTIONS ?= # --no-ansi

## Config
PHP_CS_FIXER_BIN  = ${BIN}/php-cs-fixer
PHP_CS_FIXER      = $(PHP_CLI) ${APP_BIN}/php-cs-fixer
PHP_CS_FIXER_ARGS = fix --dry-run

PHP_CS_FIXER_CONFIG = .php-cs-fixer.php
PHP_CS_FIXER_REPORT = ${REPORT}/php-cs-fixer
#
CONF_FILES += ${PHP_CS_FIXER_CONFIG}
UNINSTALL_FILES += .php_cs.cache ${PHP_CS_FIXER_BIN}

QA_TOOLS_BIN       += ${PHP_CS_FIXER_BIN}
QA_TARGETS_ANALYSE += php-cs-fixer-diff php-cs-fixer
QA_TARGETS_REPORTS += php-cs-fixer-report
##
php-cs-fixer-report: PHP_CS_FIXER_ARGS := ${PHP_CS_FIXER_ARGS} --diff
php-cs-fixer-report: ${PHP_CS_FIXER_CONFIG} ${REPORT} | ${PHP_CS_FIXER_BIN}
	$I Building PHP Code Standards Fixer reports…
	mkdir -p ${PHP_CS_FIXER_REPORT}

	$(PHP_CS_FIXER) ${PHP_CS_FIXER_ARGS} ${PHP_CS_FIXER_OPTIONS} \
		--format=json > ${PHP_CS_FIXER_REPORT}/report.json 2> /dev/null
	$(PHP_CS_FIXER) ${PHP_CS_FIXER_ARGS} ${PHP_CS_FIXER_OPTIONS} \
		--format=xml > ${PHP_CS_FIXER_REPORT}/report.xml 2> /dev/null
	$(PHP_CS_FIXER) ${PHP_CS_FIXER_ARGS} ${PHP_CS_FIXER_OPTIONS} \
		--format=checkstyle > ${PHP_CS_FIXER_REPORT}/checkstyle.xml 2> /dev/null
	$(PHP_CS_FIXER) ${PHP_CS_FIXER_ARGS} ${PHP_CS_FIXER_OPTIONS} \
		--format=junit > ${PHP_CS_FIXER_REPORT}/junit.xml 2> /dev/null
	$(PHP_CS_FIXER) ${PHP_CS_FIXER_ARGS} ${PHP_CS_FIXER_OPTIONS} \
		--format=gitlab > ${PHP_CS_FIXER_REPORT}/gitlab.json 2> /dev/null
	echo -e "Builded..."

php-cs-fixer-diff: PHP_CS_FIXER_ARGS := ${PHP_CS_FIXER_ARGS} --diff
php-cs-fixer-diff: php-cs-fixer                      ## PHP Code Standards Fixer changes to fix code

php-cs-fixer: ${PHP_CS_FIXER_CONFIG} | ${PHP_CS_FIXER_BIN}          ## PHP Code Standards Fixer
	$I PHP Code Standards Fixer…
	$(PHP_CS_FIXER) ${PHP_CS_FIXER_ARGS} ${PHP_CS_FIXER_OPTIONS}

${PHP_CS_FIXER_BIN}: | ${BIN}
	$I installing PHP CS Fixer…

	$(DOWNLOAD) ${PHP_CS_FIXER_BIN} \
		https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/download/v${PHP_CS_FIXER_VERSION}/php-cs-fixer.phar \
		|| exit $$?
	chmod +x ${PHP_CS_FIXER_BIN}
