
## Params
# INFECTION_VERSION ?= 0.25.3 # For PHP 7.4 - 8
INFECTION_VERSION ?= 0.17.7
INFECTION_OPTIONS ?=  # --coverage=build/coverage

## Config
INFECTION_BIN  = ${VENDOR_BIN}/infection
INFECTION      = $(PHP_CLI) ${INFECTION_BIN}
INFECTION_ARGS = --show-mutations --threads=4 \
                 --initial-tests-php-options="${XDEBUG_ENABLE}"
#
CONF_FILES      += infection.json
UNINSTALL_FILES += infection.log ${INFECTION_BIN}

APP_TARGETS += mutant
##
mutant: infection.json | ${INFECTION_BIN}      ## Run mutant tests
	$I running mutant tests…

	$(INFECTION) ${INFECTION_ARGS} ${INFECTION_OPTIONS}

# Tools
${INFECTION_BIN}: composer-plugins
	$I installing INFECTION…

	$(COMPOSER) bin mutant config allow-plugins.infection/extension-installer true
	$(COMPOSER) bin mutant require infection/infection &&
	touch ${INFECTION_BIN}
