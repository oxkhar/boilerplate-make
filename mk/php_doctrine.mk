
## Params
DOCTRINE_OPTIONS   ?=
MIGRATION_OPTIONS  ?=
FIXTURES_OPTIONS   ?=

## Config
DOCTRINE_BIN        = ${VENDOR_BIN}/doctrine
DOCTRINE            = $(PHP_CLI) ${APP_BIN}/doctrine

MIGRATION_BIN       = ${VENDOR_BIN}/doctrine-migrations
MIGRATION           = $(PHP_CLI) ${APP_BIN}/doctrine-migrations

FIXTURES_BIN        = cli/fixtures
FIXTURES            = $(PHP_CLI) ${APP_PATH}/cli/fixtures
#
UNINSTALL_FILES += ${DOCTRINE_BIN} ${MIGRATION_BIN}

APP_TARGETS += doctrine db-install db-uninstall db-recreate
##
db-install: doctrine | ${MIGRATION_BIN}        ## Create and initialize DB
	$I creating database…
	$(DOCTRINE) orm:schema:create ${DOCTRINE_OPTIONS}
	$I loading fixtures…
	$(FIXTURES) fixtures:load -v ${FIXTURES_OPTIONS}

db-uninstall: doctrine | ${MIGRATION_BIN}    ## Drop and remove DB schemas
	$I removing database…
	$(DOCTRINE) orm:schema:drop -f ${DOCTRINE_OPTIONS}

db-recreate: db-uninstall db-install        ## Generate and update DB schema

doctrine: | ${DOCTRINE_BIN}
	$Q
	$(DOCTRINE) orm:generate-proxies ${DOCTRINE_OPTIONS}
