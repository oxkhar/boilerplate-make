## Params
PHP_CLI_ENGINE ?= DEFAULT  # Values: LOCAL | DOCKER | DOCKER_COMPOSE

PHP_CLI        ?= $(PHP_CLI_$(strip $(PHP_CLI_ENGINE)))
PHP_DEBUG      ?= $(PHP_CLI) ${XDEBUG_ENABLE}

PHP_CLI_ARGS   ?=
XDEBUG_PARAMS  ?= ${XDEBUG3_PARAMS}

## Config
PHP_CLI_DEFAULT        = $(DOCKER_RUN) php ${PHP_CLI_ARGS}      # Run with a Docker image or PHP cli if not docker not included
# To assign to PHP_CLI and config who run PHP
PHP_CLI_LOCAL          = $(shell which php) ${PHP_CLI_ARGS}     # Run with local php
PHP_CLI_DOCKER         = $(DOCKER_CLI_RUN) php ${PHP_CLI_ARGS}  # Run with a Docker image
PHP_CLI_DOCKER_COMPOSE = $(DOCKER_COMPOSE_RUN) php ${PHP_CLI_ARGS}  # Run with a Docker Compose service

XDEBUG_ENABLE  = -dzend_extension=xdebug.so ${XDEBUG_PARAMS}
XDEBUG2_PARAMS = -dxdebug.coverage_enable=1
XDEBUG3_PARAMS = -dxdebug.mode=develop,coverage
#
APP_TARGETS += strict_types
##
strict_types:
	$I declare strict types in PHP files…
	find ${SOURCE} -name '*.php' \
	| while read PHP_FILE; do
		echo $${PHP_FILE}
		sed -e 's/declare(strict_types=1);/oooooooooooo/; s/<?php.*/<?php declare(strict_types=1);/' $${PHP_FILE} |
		grep -v 'oooooooooooo' > $${PHP_FILE}.tmp
		mv $${PHP_FILE}.tmp $${PHP_FILE}
	done
