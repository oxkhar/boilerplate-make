
## Params
PHPCPD_OPTIONS ?= # --fuzzy
PHPCPD_VERSION ?= 6.0.3

## Config
PHPCPD_BIN    = ${BIN}/phpcpd
PHPCPD        = $(PHP_CLI) ${APP_BIN}/phpcpd
PHPCPD_REPORT = ${REPORT}/phpcpd
#
UNINSTALL_FILES += ${PHPCPD_BIN}

QA_TOOLS_BIN       += ${PHPCPD_BIN}
QA_TARGETS_ANALYSE += phpcpd
QA_TARGETS_REPORTS += phpcpd-report
##
phpcpd-report: ${REPORT} | ${PHPCPD_BIN}
	$I PHP Copy/Paste Detector reports…
	mkdir -p ${PHPCPD_REPORT}

	$(PHPCPD) --log-pmd=${PHPCPD_REPORT}/report.xml ${PHPCPD_OPTIONS} ${SOURCE}
	echo -e "Builded..."

phpcpd: | ${PHPCPD_BIN}                  ## PHPCPD
	$I PHP Copy/Paste Detector…

	$(PHPCPD) ${PHPCPD_OPTIONS} ${SOURCE}

${PHPCPD_BIN}: | ${BIN}
	$I Installing PHP Copy/Paste Detector…

	$(DOWNLOAD) ${PHPCPD_BIN} \
		https://phar.phpunit.de/phpcpd-${PHPCPD_VERSION}.phar \
		|| exit $$?
	chmod +x ${PHPCPD_BIN}
