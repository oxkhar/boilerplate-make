
## Params
PHPSTAN_EXTENSIONS ?= phpstan/phpstan-beberlei-assert phpstan/phpstan-strict-rules timeweb/phpstan-enum
PHPSTAN_OPTIONS    ?= # --memory-limit=256M

## Config
PHPSTAN_BIN         = ${VENDOR_BIN}/phpstan
PHPSTAN             = $(PHP_CLI) ${APP_BIN}/phpstan
PHPSTAN_ARGS        = analyse

PHPSTAN_CONFIG      = phpstan.neon
PHPSTAN_REPORT      = ${REPORT}/phpstan
#
CONF_FILES         += ${PHPSTAN_CONFIG}
UNINSTALL_FILES    += ${PHPSTAN_BIN}

QA_TOOLS_BIN       += ${PHPSTAN_BIN}
QA_TARGETS_ANALYSE += phpstan
QA_TARGETS_REPORTS += phpstan-report
##
phpstan-report: PHPSTAN_ARGS := ${PHPSTAN_ARGS} --no-progress
phpstan-report: ${PHPSTAN_CONFIG} ${REPORT} | ${PHPSTAN_BIN}
	$I PHPStan reports…
	mkdir -p ${PHPSTAN_REPORT}

	$(PHPSTAN) ${PHPSTAN_ARGS} --error-format=raw ${PHPSTAN_OPTIONS} \
		> ${PHPSTAN_REPORT}/report.txt 2> /dev/null
	$(PHPSTAN) ${PHPSTAN_ARGS} --error-format=checkstyle ${PHPSTAN_OPTIONS}	\
	 	> ${PHPSTAN_REPORT}/checkstyle.xml 2> /dev/null
	$(PHPSTAN) ${PHPSTAN_ARGS} --error-format=json ${PHPSTAN_OPTIONS} \
		> ${PHPSTAN_REPORT}/report.json 2> /dev/null
	$(PHPSTAN) ${PHPSTAN_ARGS} --error-format=junit ${PHPSTAN_OPTIONS} \
		> ${PHPSTAN_REPORT}/junit.xml 2> /dev/null
	$(PHPSTAN) ${PHPSTAN_ARGS} --error-format=gitlab ${PHPSTAN_OPTIONS} \
		> ${PHPSTAN_REPORT}/gitlab.json 2> /dev/null
	$(PHPSTAN) ${PHPSTAN_ARGS} --error-format=baselineNeon ${PHPSTAN_OPTIONS} \
		> ${PHPSTAN_REPORT}/report.neon 2> /dev/null

	echo -e "  Built..."

phpstan: ${PHPSTAN_CONFIG} | ${PHPSTAN_BIN}            ## PhpStan
	$I PHPStan…

	$(PHPSTAN) ${PHPSTAN_ARGS} ${PHPSTAN_OPTIONS}

${PHPSTAN_BIN}: | composer-plugins
	$I installing PHPStan…
	$(COMPOSER) bin phpstan config allow-plugins.phpstan/extension-installer true
	$(COMPOSER) bin phpstan require phpstan/phpstan phpstan/extension-installer ${PHPSTAN_EXTENSIONS} &&
	touch ${PHPSTAN_BIN}
