<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$config = new Config();
return $config
    ->setCacheFile(__DIR__.'/var/cache/php_cs.cache')
    ->setRules(
        [
            '@PSR2' => true,
            'array_syntax' => ['syntax' => 'short'],
            'phpdoc_align' => false,
        ]
    )
    ->setFinder(
        Finder::create()
            ->in(__DIR__.'/src')
    );
