
function help() {
  local helpToShow="${1:-}"

  local helpAvailableBuddies=($(_aliases-buddies-included))
  _shell-is-interactive ||  # If not in interactive mode
    helpAvailableBuddies+=(shell) # Add shell to the help list
  ## Load help modules
  for h in "${helpAvailableBuddies[@]}"; do
    [ -e "${ALIASES_PATH}/help-${h}.sh" ] && source "${ALIASES_PATH}/help-${h}.sh"
    say-debug "Loading help for buddie '${h}'"
  done

  # If is set a specific help to show, then show the extended help
  local helpOption=$([ -z "${helpToShow}" ] || echo "extended")
  ## show help for a specific topic or all when no topic or "all" is provided
  helpToShow=${helpToShow,,} # To lowercase
  [ -z "${helpToShow}" ] || [ "${helpToShow}" == "all" ] && helpToShow="${helpAvailableBuddies[*]}"

  ## Print help
  _help-print "${helpOption}" "${helpToShow}"
}

_help-print() {
  local helpOption="${1:-default}"
  local helpToShow="${2? Which help do you want to show?}"
  say-debug "View help with option '${helpOption}' to show: ${helpToShow}"

  local HELP_CMD=""
  _shell-is-interactive ||
    HELP_CMD="$(basename "${RUN_BIN}")" # To show the script tool name in the help when is not in interactive mode

  _shell-is-interactive ||
    echo -en "\nModo de empleo: ${HELP_CMD} <ACCION> [ARGS] …\n"
  for h in ${helpToShow}; do
    text-colorize bold "\n$(help-title-${h} 2>/dev/null ||
      echo -n " ‣ No hay ayuda disponible para $(text-colorize bold fg_red  "${h}")")\n" &&
      help-info-${h} ${helpOption} 2>/dev/null || true
  done

  cat <<EOT

$(text-colorize bold "Ayuda adicional…")
    help            Muestra ayuda general del proyecto
    help all        Muestra ayuda detallada de todas las acciones
$(for h in "${helpAvailableBuddies[@]}";  do
    help-about-${h} 2>/dev/null
done)

EOT
}