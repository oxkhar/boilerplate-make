
##
config-core-runner() {
  var ALIASES_RUNNER ""
  var ALIASES_BY_DEFAULT "help"

  var TOOL_WAIT_FOR_IT_BIN "${ALIASES_PATH}/tool-wait-for-it"
}

###
##
run() {
  say-debug "${ALIASES_RUNNER} ${*}"
  ${ALIASES_RUNNER} "$@"
}

lets-go() {
  [ $# -eq 0 ] && lets-go "${ALIASES_BY_DEFAULT}" && return
  local commandType=$(type -t "${1}")
  [ -z "${commandType}" ] && say-error "No way to execute '${1}'" && return 1
  local exitCode=0
  say-debug "${*}"
  "$@" || exitCode=$?
  say-debug "Excuted '${1}' as ${commandType} with exit code ${exitCode}"
  return ${exitCode}
}

wait-for-it() {
  local hostAndPort=${1:?Which host do you want to wait for? (host:port)}
  local timeout=${2:-60}

  say-info "Waiting for ${hostAndPort} to be available"

  run ${TOOL_WAIT_FOR_IT_BIN} ${hostAndPort} --timeout=${timeout}
}

