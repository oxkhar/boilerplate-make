
config-php() {
  var VENDOR_PATH "${PROJECT_PATH}/vendor"
  var VENDOR_BIN "${VENDOR_PATH}/bin"

  var PHP_COMPOSER_BIN "${PROJECT_BIN}/composer"
  var PHP_COMPOSER_VERSION "2.7.2"

  var PHPUNIT_BIN "${VENDOR_BIN}/phpunit"
  var PHP_CS_BIN "${VENDOR_BIN}/php-cs-fixer"
  var PHPSTAN_BIN "${VENDOR_BIN}/phpstan"
  var PHPARKITECT_BIN "${VENDOR_BIN}/phparkitect"
}

###
php-cli() {
  run php "$@"
}

php-debug() {
  php-cli \
    -dzend_extension=xdebug.so \
    -dxdebug.mode=develop,debug,coverage \
    -dxdebug.start_with_request=yes \
    -dxdebug.client_host=${XDEBUG_REMOTE_PORT:-172.17.0.1} \
    -dxdebug.client_port=${XDEBUG_REMOTE_HOST:-9003} \
    "$@"
}

php-xdebug2() {
  php-cli \
    -dzend_extension=xdebug.so \
    -dxdebug.coverage_enable=1 \
    -dxdebug.remote_enable=1 \
    -dxdebug.remote_autostart=1 \
    -dxdebug.remote_port=${XDEBUG_REMOTE_PORT:-172.17.0.1} \
    -dxdebug.remote_host=${XDEBUG_REMOTE_HOST:-9000} \
    "$@"
}

composer() {
  composer-install

  DOCKER_ARGS+=(--volume composer:/home/composer --env COMPOSER_HOME=/home/composer)
  php-cli "${PHP_COMPOSER_BIN}" "$@"
}

vendors() {
  composer install --ignore-platform-reqs --no-interaction --no-scripts --no-progress --prefer-dist
}

phpunit() {
  php-cli "${PHPUNIT_BIN}" "$@"
}

php-cs-fixer() {
  php-cli "${PHP_CS_BIN}" "$@"
}

phpstan() {
  php-cli "${PHPSTAN_BIN}" "$@"
}

phparkitect() {
  php-cli "${PHPARKITECT_BIN}" "$@"
}

### Tools
composer-init() {
  composer init --no-interaction \
    --type project \
    --license WTFPL \
    --stability stable \
    --name "${1:-${APP_PROJECT}/${APP_NAME}}"
}

composer-install() {
  [ -e "${PHP_COMPOSER_BIN}" ] && return
  say-info "Installing composer ${PHP_COMPOSER_VERSION} to ${PHP_COMPOSER_BIN}"

  local urlDownload="https://getcomposer.org/download/${PHP_COMPOSER_VERSION}/composer.phar"

  download "${PHP_COMPOSER_BIN}" "${urlDownload}" || return $?
  chmod +x "${PHP_COMPOSER_BIN}"
}
