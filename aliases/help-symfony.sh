
help-title-symfony() {
cat <<EOT
 ‣ Symfony…
EOT
}

help-about-symfony() {
cat <<EOT
    help symfony    Ayuda de las acciones de Symfony
EOT
}

help-info-symfony() {
  case ${1:-} in
    extended)
cat <<EOT
    console         Ejecuta comandos de Symfony
    cache           Limpia y calienta la caché de Symfony
    consumer        Consume los mensajes de la cola de mensajes

      Para las acciones que se ejecutan a través de la consola de Symfony se puede utilizar la opción --env para indicar el entorno de ejecución:
        ${HELP_CMD} console --env test
        ${HELP_CMD} cache --env test
        ${HELP_CMD} fixtures --env test
EOT
    ;;
    *)
cat <<EOT
    console         Ejecuta comandos de Symfony
EOT
      ;;
  esac
}
