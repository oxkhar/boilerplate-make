
help-title-asm() {
cat <<EOT
 ‣ Z80 ASM…
EOT
}

help-about-asm() {
cat <<EOT
    help asm   Ayuda de las acciones de Z80 ASM
EOT
}

help-info-asm() {
  case ${1:-} in
    extended)
cat <<EOT
    pasmo           Ejecuta el compilador de Z80
    rvm             Ejecuta el emulador de Z80

    play            Compila y ejecuta un programa Z80

EOT
    ;;

    *)
cat <<EOT
    pasmo           Ejecuta el compilador de Z80
    rvm             Ejecuta el emulador de Z80
    play            Compila y ejecuta un programa Z80
EOT
    ;;
  esac
}
