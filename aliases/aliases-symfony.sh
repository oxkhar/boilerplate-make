
config-symfony() {
  var CONSOLE_BIN "${PROJECT_BIN}/console"
}
###
function console() {
  run "${CONSOLE_BIN}" "$@"
}

function cache() {
  console cache:clear "$@"
  console cache:warmup "$@"
}

function consumer() {
  console messenger:consume -vv "${@}"
}

function wait-for() {
  local HOST_ENV=${1%%:*}
  local PORT_ENV=${1##*:}
  shift

  wait-for-it "$(.sf-env ${HOST_ENV} "${@}"):$(.sf-env ${PORT_ENV} "${@}")"
}

function .sf-env() {
  local ENV_KEY=${1:?Which key do you want to get the value of?}
  shift

  console debug:dotenv --no-ansi "${@}" | grep "${ENV_KEY}" | sed "s/.*${ENV_KEY}[ ]*//" | cut -f 1 -d ' '
}

## Experimental
function shell-console() {
  run shell-run console list
}
