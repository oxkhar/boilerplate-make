
download() {
  local fileToDownload=${1:?Where do you want to save the file?}
  local urlWhereDownload=${2:?Which URL do you want to download?}

  say-debug "Downloading ${urlWhereDownload} to ${fileToDownload}"

  curl -sSfL --progress-bar -o ${fileToDownload} ${urlWhereDownload}
}

env-dev() {
  [[ "$1" == "" || ! -e "$1" ]] && return

  local APP_DEBUG=${APP_DEBUG:-true}
  local APP_VERSION=${APP_VERSION:-latest}

  cat "$@" 2>/dev/null |
    sed \
      -e "s/APP_DEBUG=.*/APP_DEBUG=${APP_DEBUG}/" \
      -e "s/APP_VERSION=.*/APP_VERSION=${APP_VERSION}/"
}

env-parse() {
  [[ "$1" == "" ]] && return

  local APP_DEBUG=${APP_DEBUG:-true}
  local APP_VERSION=${APP_VERSION:-latest}

  cat "$@" | grep -v "^#" |
    sed \
      -e 's/\&/{%26}/g' \
      -e 's/\\/\\\\/g' -e 's/"/\\"/g' -e "s/'/\\\'/g" \
      -e 's/(/\\(/g' -e 's/)/\\)/g' -e 's/|/\\|/g' \
      -e "s/APP_DEBUG=.*/APP_DEBUG=${APP_DEBUG}/" \
      -e "s/APP_VERSION=.*/APP_VERSION=${APP_VERSION}/" |
    BASH_XTRACEFD=1 PS4='' bash -x |
    sed \
      -e 's/{%26}/\&/g' -e "s/^\+ //g" -e "s/='/=/g" \
      -e "s/'$//g" -e "s/'\\\''/'/g" |
    sort --unique
}
