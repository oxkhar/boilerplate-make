
help-title-database() {
cat <<EOT
 ‣ Base de datos…
EOT
}

help-about-database() {
cat <<EOT
    help database   Ayuda de las acciones de Base de datos
EOT
}

help-info-database() {
  case ${1:-} in
    extended)
cat <<EOT
    database        Recrea la base de datos
    schemas         Recrea el esquema de la base de datos

    fixtures        Carga los datos de prueba en la base de datos

    migrate         Ejecuta las migraciones de la base de datos
    migration-diff  Genera migraciones con las diferencias de la base de datos

      Se puede especificar el entorno de ejecución para que los cambios se apliquen en un entorno específico.
      Por ejemplo, para ejecutar las migraciones en el entorno de tests...
        ${HELP_CMD} schemas   # entorno 'dev' por defecto
        ${HELP_CMD} fixtures --env test
        ${HELP_CMD} migrate --env dev
EOT
    ;;

    *)
cat <<EOT
    schemas         Recrea el esquema de la base de datos
    fixtures        Carga los datos de prueba en la base de datos
    migrate         Ejecuta las migraciones de la base de datos
EOT
    ;;
  esac
}
