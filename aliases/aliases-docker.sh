
config-docker() {
  docker-env-load

  var DOCKER_BIN "$(which docker)"
  var DOCKER_FILE ${PROJECT_PATH}/Dockerfile
  var DOCKER_BUILD_TARGET ""

  var UID "$(id -u)"
  var GID "$(id -g)"
  var DOCKER_APP_USER "$(id -un)"

  var DOCKER_RUN_USER ${UID}:${GID}
  var DOCKER_RUN_IMAGE ${DOCKER_REGISTRY:=}${DOCKER_IMAGE_NAME:=php}:${DOCKER_IMAGE_VERSION:=latest}
  var DOCKER_RUN_NETWORK bridge

  var DOCKER_APP_PATH ${APP_PATH:-/app}
  var DOCKER_RUN_BIN ${RUN_BIN/${PROJECT_PATH}/${DOCKER_APP_PATH}}

  ## Docker Compose
  var DOCKER_COMPOSE_BIN "${PROJECT_BIN}/docker-compose"
  var DOCKER_COMPOSE_VERSION "2.26.0"

  var DOCKER_COMPOSE_FILE "${PROJECT_PATH}/compose.yaml"
  var DOCKER_SERVICE_RUN "application"

  ## Docker Runner
  [[ -n "${DOCKER_BIN}" ]] && var ALIASES_RUNNER "docker-run"
  [[ -e "${DOCKER_COMPOSE_FILE}" ]] && var DOCKER_RUNNER "docker-compose-run" || var DOCKER_RUNNER "docker-cli-run"

  DOCKER_BUILD_ARGS+=()
  DOCKER_ARGS+=()
  DOCKER_COMPOSE_ARGS+=()
}

###
start() {
  docker-compose up -d "$@"
}

stop() {
  docker-compose down "$@"
}

restart() {
  stop "$@"
  start "$@"
}

logs() {
  docker-compose logs -f "$@"
}

kill-em-all() {
  local containers=$(${DOCKER_BIN} container ls -q)
  echo ${containers} | xargs --no-run-if-empty ${DOCKER_BIN} container stop
}

##
docker-build() {
  [ "${DOCKER_BIN}" == "" ] && return
  local dockerBuildTarget=${1:-${DOCKER_BUILD_TARGET}}

  local defaultBuildArgs=(
      "${DOCKER_BUILD_ARGS[@]}"
      --build-arg UID="${UID}"
      --build-arg GID="${GID}"
      --build-arg APP_USER="${DOCKER_APP_USER}"
      --build-arg APP_PATH="${DOCKER_APP_PATH}"
  )
  DOCKER_BUILD_ARGS=()

  if [[ -e "${DOCKER_COMPOSE_FILE}" && -z "${dockerBuildTarget}" ]]; then
    say-info "Building Docker image for service ${DOCKER_SERVICE_RUN}\n"
    docker-compose build "${defaultBuildArgs[@]}"
    return $?
  fi

  if [[ -e "${DOCKER_FILE}" ]]; then
    [ -n "${dockerBuildTarget}" ] && defaultBuildArgs+=(--target "${dockerBuildTarget}")
    defaultBuildArgs+=(
      --file "${DOCKER_FILE}"
      --tag "${DOCKER_RUN_IMAGE}"
    )
    say-info "Building Docker image ${DOCKER_RUN_IMAGE} from target ${dockerBuildTarget:-__default__}"
    docker-cli build "${defaultBuildArgs[@]}" "${PROJECT_PATH}"
    return $?
  fi
}

docker-run() {
  ssh-auth-sock

  DOCKER_ARGS+=(
    --volume "${SSH_AUTH_SOCK}":/ssh-auth.sock --env SSH_AUTH_SOCK=/ssh-auth.sock
    --volume "${PROJECT_PATH}":"${DOCKER_APP_PATH}"
    --rm --entrypoint ""
    --user "${DOCKER_RUN_USER}"
  )
  # Change local paths to remote paths
  ${DOCKER_RUNNER} "${@//${PROJECT_PATH}/${DOCKER_APP_PATH}}"
}

docker-network() {
  docker-cli network create ${DOCKER_RUN_NETWORK} 2>/dev/null
}
### Docker CLI
function docker-cli-run() {
  tty --silent && DOCKER_ARGS+=(--tty --interactive) || DOCKER_ARGS+=(--attach)

  [ -e "${DOCKER_ENV}" ] && DOCKER_ARGS+=(--env-file "${DOCKER_ENV}")

  docker-cli run "${DOCKER_ARGS[@]}" "${DOCKER_RUN_IMAGE}" bash "${DOCKER_RUN_BIN}" "$@"
}

docker-cli() {
  docker-install

  DOCKER_ARGS=()
  say-debug "Running… ${DOCKER_BIN} ${*}"
  ${DOCKER_BIN} "${@}"
}

### Docker Compose
docker-compose-run() {
  tty --silent && DOCKER_ARGS+=(--interactive) || DOCKER_ARGS+=(--no-TTY)

  DOCKER_ARGS+=(--no-deps)

  docker-compose run "${DOCKER_ARGS[@]}" ${DOCKER_SERVICE_RUN} bash "${DOCKER_RUN_BIN}" "$@"
}

docker-compose() {
  DOCKER_ARGS=()
  [ ! -e "${DOCKER_COMPOSE_FILE}" ] && return
  docker-compose-install

  DOCKER_COMPOSE_ARGS=(--file "${DOCKER_COMPOSE_FILE}")
  [ -e "${DOCKER_ENV}" ] && DOCKER_COMPOSE_ARGS+=(--env-file "${DOCKER_ENV}")

  say-debug "Running… ${DOCKER_COMPOSE_BIN} ${DOCKER_COMPOSE_ARGS[*]} ${*}"
  ${DOCKER_COMPOSE_BIN} "${DOCKER_COMPOSE_ARGS[@]}" "${@}"
}

docker-compose-install() {
  docker-install
  docker-env
  [ -e "${DOCKER_COMPOSE_BIN}" ] && return

  say-info "Installing docker-compose ${DOCKER_COMPOSE_VERSION} to ${DOCKER_COMPOSE_BIN}"
  download ${DOCKER_COMPOSE_BIN} \
    "https://github.com/docker/compose/releases/download/v${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" ||
    return $?
  chmod +x ${DOCKER_COMPOSE_BIN}
}

docker-env-load() {
  var DOCKER_DIST_ENV "${PROJECT_PATH}/docker.dist.env"
  var DOCKER_ENV "${DOCKER_DIST_ENV//.dist/}"

  docker-env generate

  [ -e "${DOCKER_ENV}" ] && source "${DOCKER_ENV}"
}

docker-env() {
  [[ ! -e "${DOCKER_DIST_ENV}" ]] && return
  [[ -e "${DOCKER_ENV}" && "${1:-}" != "overwrite" ]] && return

  local dockerEnvVars=($(ls -U "${PROJECT_PATH}/.env" "${PROJECT_PATH}/.env.${APP_ENV:-dev}" 2> /dev/null))
  local filterEnvFile="/tmp/aliases-env-filter-vars"

  say-info "Generating Docker environment file ${DOCKER_ENV}"

  echo "# Docker environment" > "${DOCKER_ENV}"
  sed 's/=.*//;s/^#.*//' "${DOCKER_DIST_ENV}" | grep -v '^$' > "${filterEnvFile}"

  env-parse "${dockerEnvVars[@]}" "${DOCKER_DIST_ENV}" | grep -Fwf "${filterEnvFile}" >> ${DOCKER_ENV}

  [ -z "${dockerEnvVars[*]}" ] && return

  echo "# App environment" >> ${DOCKER_ENV}
  cat "${dockerEnvVars[@]}" | sed 's/^#.*//;s/=.*//' | grep -v '^$' > "${filterEnvFile}"
  env-parse "${dockerEnvVars}" "${DOCKER_DIST_ENV}" | grep -Fwf "${filterEnvFile}" >> ${DOCKER_ENV}
}

ssh-auth-sock() {
  local OS_TYPE=$(uname -s)

  if [[ "${OS_TYPE}" == "Linux" && "${SSH_AUTH_SOCK}" == "" ]]; then
    # This sets SSH_AUTH_SOCK to the socket file path
    eval "$(ssh-agent -s)"
    ssh-add ~/.ssh/*
  fi

  if [[ "${OS_TYPE}" == "Darwin" ]]; then
    # If using Rancher Desktop
    if [ -x "$(which rdctl)" ]; then
      export SSH_AUTH_SOCK=$(rdctl shell printenv SSH_AUTH_SOCK)
    # If using colima
    elif [ -x "$(which colima)" ]; then
      export SSH_AUTH_SOCK=$(colima ssh printenv SSH_AUTH_SOCK)
    else
      echo "Unsupported Docker for Mac"
      return 1
    fi
  fi
}

docker-install() {
  [ "${DOCKER_BIN}" != "" ] && [ -e "${DOCKER_BIN}" ] && return

  say-info Installing docker…
  download ${PROJECT_BIN}/get-docker.sh https://get.docker.com
  sh ${PROJECT_BIN}/get-docker.sh || false
  sleep 1
  rm -f ${PROJECT_BIN}/get-docker.sh
  sudo adduser $(whoami) docker
  sg docker "docker ps -q" || false
  sg docker "${RUN_BIN} docker-build"
  say-warning "Restart your session to apply group changes and current user can use Docker"
  sg docker "newgrp $(whoami)" && false
}