
help-title-version() {
cat <<EOT
 ‣ Versionado…
EOT
}

help-about-version() {
cat <<EOT
    help version    Ayuda de las acciones de Versión
EOT
}

help-info-version() {
  case ${1:-} in
    *)
cat <<EOT
    tag             Crea una nueva etiqueta de versión y la publica en el repositorio.
                    La versión debe ser especificada como argumento…
                      ${HELP_CMD} tag x.x.x
EOT
    ;;
  esac
}
