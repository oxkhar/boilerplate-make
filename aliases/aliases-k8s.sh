## Experimental Kubernetes support

config-k8s() {
  var K8S_CONFIG "${HOME}/.kube/config"
  var K8S_NAMESPACE "${PROJECT_NAME:-default}"
  var K8S_KUBECTL_BIN "${PROJECT_BIN}/kubectl"
  var K8S_KUBECTL_VERSION "1.26.12"
}

function kubectl() {
  k8s-kubectl-install

  ${K8S_KUBECTL_BIN} --kubeconfig="${K8S_CONFIG}" --namespace="${K8S_NAMESPACE}" "$@"

}

function k8s-ls() {
  kubectl get all
}

function k8s-kubectl-install() {
  [ -e "${K8S_KUBECTL_BIN}" ] && return

  say-info "Installing kubectl ${K8S_KUBECTL_VERSION} to ${K8S_KUBECTL_BIN}"
  download ${K8S_KUBECTL_BIN} \
    "https://dl.k8s.io/release/v${K8S_KUBECTL_VERSION}/bin/linux/amd64/kubectl" ||
    return $?
  chmod +x ${K8S_KUBECTL_BIN}
}
