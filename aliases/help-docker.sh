
help-title-docker() {
cat <<EOT
 ‣ Docker…
EOT
}

help-about-docker() {
cat <<EOT
    help docker     Ayuda de las acciones de Docker
EOT
}

help-info-docker() {
  case ${1:-} in
    extended)
      [ -e "${DOCKER_COMPOSE_FILE}" ] && cat <<EOT
    start           Inicia la ejecución del proyecto con todos los servicios necesarios
    stop            Finaliza la ejecución del proyecto
    restart         Reinicia la ejecución del proyecto con todos los servicios necesarios

EOT
cat <<EOT
    docker-build    Construye la imagen de Docker
    docker-run      Ejecuta un comando dentro del contenedor de Docker
    docker-compose  Ejecución de docker-compose

    kill-em-all     Busca cualquier contenedor en ejecución y serán parados todos

      Se puede ejecutar cualquier comando dentro del contenedor a través de..
        ${HELP_CMD} docker-run php -v
        ${HELP_CMD} docker-run ls -la
EOT
    ;;
    *)
      [ -e "${DOCKER_COMPOSE_FILE}" ] && cat <<EOT
    start           Inicia la ejecución del proyecto con todos los servicios necesarios
    stop            Finaliza la ejecución del proyecto
    restart         Reinicia la ejecución del proyecto con todos los servicios necesarios

EOT
cat <<EOT
    kill-em-all     Busca cualquier proyecto y para la ejecución de docker-compose
EOT
    ;;
  esac
}
