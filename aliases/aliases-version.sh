
config-version() {
  var CHAG_BIN "${PROJECT_BIN}/chag"
  var CHAG_VERSION "1.1.4"
}

###
function tag() {
  local TAG=${1:?"necesario especificar una version de etiqueta (x.x.x)"}
  chag-install

  ${CHAG_BIN} update ${TAG}
  git add CHANGELOG.md
  echo -e "\n▶" Tag .env…
  sed -e "s/APP_VERSION*=.*/APP_VERSION=${TAG}/" .env > .env.tag && cat .env.tag > .env && rm .env.tag
  git add .env
  echo -e "\n▶" Tag version file…
  echo ${TAG} > VERSION
  git add VERSION

  echo -e "\n▶" Init release ${TAG}…
  git checkout -B "release-${TAG}"
  git commit -m "${TAG} release"

  echo -e "\n▶" Merge into main branch master…
  git fetch --all --prune
  git checkout -B master origin/master
  git merge --no-ff -m "merge release ${TAG} into master" "release-${TAG}"
  git branch -d "release-${TAG}"

  echo -e "\n▶" Creating tag ${TAG}…
  ${CHAG_BIN} tag --force
}

function chag-install() {
  [ -e "${CHAG_BIN}" ] && return

  say-info "Installing chag ${CHAG_VERSION} to ${CHAG_BIN}"
  download "${CHAG_BIN}" \
    "https://github.com/mtdowling/chag/releases/download/${CHAG_VERSION}/chag" \
    || reutrn $?
  chmod +x ${CHAG_BIN}
}
