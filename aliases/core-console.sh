### Console formatter & colors

function say-debug() {
  aliases-debug &&
    text-colorize fg_purple "DEBUG🔈" >&2 &&
    text-colorize bold "${@}\n" >&2 || true
}

function say-info() {
  text-colorize fg_blue "INFO🔈" >&2
  text-colorize bold "${@}\n" >&2
}

function say-warning() {
  text-colorize fg_yellow "WARN🔉" >&2
  text-colorize bold "${@}\n" >&2
}

function say-error() {
  text-colorize fg_red bold "FAIL🔊" >&2
  text-colorize bold "${@}\n" >&2
}

function text-capitalize() {
  local words="${*,,}"
  local sep=""
  for w in ${words//[[:punct:]]/ }; do
    echo -n "${sep}${w^}"
    sep=" "
  done
}

function text-format_if() {
  local condition=${1?Condition is required}
  local name_value=${2?Variable name is required}
  local format=${3:-}
  local color=${4:-}

  [[ $condition == "not_empty" && ${!name_value} == "" ]] && return 1
  [[ $condition == "not_zero"  && ${!name_value} -eq 0 ]] && return 1

  [ -n "$color" ] && local color_${name_value}=${color}

  text-format $name_value "$format"
}

function text-format() {
  local name_value=${1?Variable name is required}
  local name_color="color_${name_value}"
  local name_prompt="prompt_${name_value}"
  local format=${2:-"{prompt}{value}"}

  format=${format//"{value}"/${!name_value:-}}
  format=${format//"{prompt}"/${!name_prompt:-}}

  text-colorize -p ${!name_color} "${format}"
}

function text-display_if() {
  local condition=${1?Condition is required}
  local value=${2?Value is required}
  local format=${3:-"{value}"}
  local color=${4:-"reset"}

  [[ $condition == "not_empty" && ${value} == "" ]] && return 1
  [[ $condition == "not_zero"  && ${value} -eq 0 ]] && return 1

  format=${format//"{value}"/${value:-}}

  text-colorize -p ${color} "${format}"
}

function text-colorize() {
  # special
  local -i  \
      reset=0         \
      bold=1          \
      underline=4
  #foreground
  local -i  \
      fg_black=30     \
      fg_red=31       \
      fg_green=32     \
      fg_yellow=33    \
      fg_blue=34      \
      fg_purple=35    \
      fg_cyan=36      \
      fg_white=37
  #background
  local -i  \
      bg_black=40     \
      bg_red=41       \
      bg_green=42     \
      bg_yellow=43    \
      bg_blue=44      \
      bg_purple=45    \
      bg_cyan=46      \
      bg_white=47

  local for_prompt=""
  [[ "${1}" == "-p" ]] && for_prompt="true" && shift

  local -i num_params=${#@}
  local text=${1}
  local color=$reset
  local code=''

  while [[ -n "${2:-}" ]]; do
      code=${!1:-0}
      if [[ -n "${code}" ]]; then
          (( code == 1 || code == 0 )) &&
              color=${color/?/$code} ||
              color+=";${code}"
      fi
      text=${2}
      shift
  done

  if (( num_params > 1 )); then
      color=(
        "\e[${color}m"
        "\e[${reset}m"
      )
      [[ -n $for_prompt ]] && color=(
        "\[${color[0]}\]"
        "\[${color[1]}\]"
      )
      text="${color[0]}${text}${color[1]}"
  fi

  echo -en "${text}"
}
