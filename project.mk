
## Params
APP_PROJECT ?= project
APP_NAME    ?= name
APP_VERSION ?= latest

## Paths into container
APP_PATH    ?= .
APP_BIN      = ${APP_PATH}/${BIN}
## Paths out container
# ...

#
DATA_PATH   ?= var/data
DATA_DIRS   ?= # mysql sqlite mongodb (for example DB data paths)

CACHE_PATH  ?= var/cache
LOG_PATH    ?= var/log
TMP_PATH    ?= var/tmp

MK_DIRS     += $(DATA_DIRS:%=${DATA_PATH}/%) ${CACHE_PATH} ${LOG_PATH} ${TMP_PATH}

CLEAN_FILES     += ${CACHE_PATH}/* ${TMP_PATH}/*
UNINSTALL_FILES += var $(shell ls ${BIN}/* | grep -v "/console$$")

## Add new specific targets for build project hers...
#
