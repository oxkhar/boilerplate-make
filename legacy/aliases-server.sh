##
: ${PHP_SERVER_CLI:=php-dbg}
: ${PHP_SERVER_HOST:=php-server}
: ${PHP_SERVER_PORT:=8000}
: ${PHP_SERVER_PORT_HOST:=${PHP_SERVER_PORT}}
: ${PHP_SERVER_ROOT:=public}

##
php-server() {
  local DOCKER_RUN_OPTIONS="
    ${DOCKER_RUN_OPTIONS}
    --detach
    --publish ${PHP_SERVER_PORT_HOST}:${PHP_SERVER_PORT}
    --name ${PHP_SERVER_HOST}
"

  if [[ "$1" == "stop" ]]; then
    ${DOCKER_CLI} container stop ${PHP_SERVER_HOST}
  elif [[ "$1" == "logs" ]]; then
    ${DOCKER_CLI} container logs -f ${PHP_SERVER_HOST}
  else
    ${PHP_SERVER_CLI} \
      -S 0.0.0.0:${PHP_SERVER_PORT} \
      -t ${PHP_SERVER_ROOT}

    echo -e "\n\n  Run... http://localhost:${PHP_SERVER_PORT_HOST}/ \n    * php-server stop  (Stop server) \n    * php-server logs  (See logs) \n"
  fi
}

##
aliases-var PHP_SERVER_CLI PHP_SERVER_HOST PHP_SERVER_PORT PHP_SERVER_PORT_HOST PHP_SERVER_ROOT
aliases-fnc php-server
aliases-alias composer phpunit phpunit-coverage kahlan behat phpstan console
