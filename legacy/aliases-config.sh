##

aliases-config() {
  : ${ENV_PRELOAD_FILES:=""}
  : ${APP_ENV_CONFIG:=".env"}
  : ${APP_ENV_FILE:=".env.local"}
  : ${DOCKER_PRELOAD_FILES:="${APP_ENV_FILE}"}
  : ${DOCKER_ENV_CONFIG:="docker.env"}
  : ${DOCKER_ENV_FILE:=".env.docker"}

  [[ ! -e "${APP_ENV_FILE}" && -e "${DOT_ENV_FILE}" ]] &&
    aliases-env ${DOT_PRELOAD_FILES} ${DOT_ENV_FILE} >"${APP_ENV_FILE}"
  [[ -e "${APP_ENV_FILE}" ]] && . "${APP_ENV_FILE}"

  [[ -e "${DOCKER_DOT_ENV_FILE}" ]] &&
    aliases-docker-env ${DOCKER_PRELOAD_FILES} ${DOCKER_DOT_ENV_FILE} >"${DOCKER_ENV_FILE}"
  [[ -e "${DOCKER_ENV_FILE}" ]] && . "${DOCKER_ENV_FILE}"

  : ${TOOLS_BIN:=bin}
  : ${VENDOR_BIN:=bin}
}

##
aliases-var DOT_PRELOAD_FILES DOT_ENV_FILE APP_ENV_FILE DOCKER_PRELOAD_FILES DOCKER_DOT_ENV_FILE DOCKER_ENV_FILE TOOLS_BIN VENDOR_BIN
