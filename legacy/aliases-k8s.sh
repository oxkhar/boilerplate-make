##
: ${K8S_CLI:="${TOOLS_BIN}/kubectl"}
: ${K8S_KUBECTL_VERSION:="1.27.1"}
: ${K8S_TELEPRESENCE_CLI:="${TOOLS_BIN}/telepresence"}
: ${K8S_TELEPRESENCE_VERSION:="latest"}

: ${K8S_CONTEXT:="default"}
: ${K8S_NAMESPACE:="default"}

##
kubectl-download() {
  [ -e "${K8S_CLI}" ] && return 0
  echo installing kubectl…
  aliases-download "${K8S_CLI}" https://dl.k8s.io/release/v${K8S_KUBECTL_VERSION}/bin/linux/amd64/kubectl || return $?
  chmod +x "${K8S_CLI}"
  grep -qs "kubectl" ~/.bash_aliases || echo -e "\n"'. <(kubectl completion bash)'"\n" >>~/.bash_aliases
}

telepresence-download() {
  [ -e "${K8S_TELEPRESENCE_CLI}" ] && return 0
  echo installing telepresence…
  aliases-download "${K8S_TELEPRESENCE_CLI}" \
    https://app.getambassador.io/download/tel2/linux/amd64/${K8S_TELEPRESENCE_VERSION}/telepresence || return $?
  chmod +x "${K8S_TELEPRESENCE_CLI}"
  grep -qs "telepresence" ~/.bash_aliases || echo -e "\n"'. <(telepresence completion bash)'"\n" >>~/.bash_aliases
}

k8s() {
  export K8S_${1}="${2}"
}

kubectl() {
  aliases-exec "${K8S_CLI}" --context="${K8S_CONTEXT}" --namespace="${K8S_NAMESPACE}" "$@"
}

telepresence() {
  aliases-exec "${K8S_TELEPRESENCE_CLI}" "$@"
}

aliases-var K8S_CLI K8S_KUBECTL_VERSION K8S_CONTEXT K8S_NAMESPACE K8S_TELEPRESENCE_CLI K8S_TELEPRESENCE_VERSION
aliases-fnc kubectl kubectl-download telepresence-download telepresence
aliases-alias
