
## Params
APP_ENV_CONFIG ?= .env
APP_ENV_FILE   ?= .env.local
DOT_ENV_FILES  ?= ${APP_ENV_CONFIG}

## Config
CONFIG_TOOLS  = ${CONFIG}/tools
#
MK_DIRS      +=  # Create directories
DIST_FILES   +=  # Create file from dist file (*.dist)
CONF_FILES   +=  # Copy files from config dir
#
INSTALL_TARGETS := env install update make_dirs clean uninstall
DEVELOP_TARGETS += install-dev-env
APP_TARGETS     += ${INSTALL_TARGETS} ${DEVELOP_TARGETS}

CLEAN_FILES     += ${BUILD}
UNINSTALL_FILES += ${APP_ENV_FILE} ${MK_DIRS} ${DIST_FILES} ${CONF_FILES}
##
${DEVELOP_TARGETS}: APP_VERSION=latest
${DEVELOP_TARGETS}: APP_DEBUG=true

latest: APP_VERSION=latest
latest: env

env:
	$I environment ${APP_ENV}…
	. ${MKUP_PATH}/aliases/core-system.sh
	env-dev ${DOT_ENV_FILES} > "${APP_ENV_FILE}"

install: make_dirs               ## Project initialization

install-dev-env: env             ## Install develop environment
	make install

${MK_DIRS}:
	mkdir -p ${@} && chmod 777 ${@}

${DIST_FILES}:
	cp ${@:%=%.dist} ${@}

${CONF_FILES}:
	$I Installing ${@F} config…
	cp ${CONFIG}/${@F} ${@} 2> /dev/null ||          # Try co copy from config files in project
	cp ${CONFIG_TOOLS}/${@F} ${@} 2> /dev/null ||    #
	cp ${MKUP_PATH}/config/${@F} ${@} 2> /dev/null || # If not found copy default config in make tools
	cp ${MKUP_PATH}/config/tools/${@F} ${@}

make_dirs: ${MK_DIRS}

clean:                          ## Cleanup building artifacts
	$I cleaning…
	for f in ${CLEAN_FILES}; do
		f=${CURDIR}/$${f/#\/}
		[ -e "$$f" ] && echo -e "   $M $${f/#'${CURDIR}/'}" && rm -r "$$f" || true
	done

uninstall: clean                ## Reset project installation
	$I undo installation…
	for f in ${UNINSTALL_FILES}; do
		f=${CURDIR}/$${f/#\/}
		[ -e "$$f" ] && echo -e "   $M $${f/#'${CURDIR}/'}" && rm -r "$$f" || true
	done

.PHONY: ${APP_TARGETS}
